��            )   �      �  $   �  +   �  #   �  V     +   m  
   �     �  A   �  
   �     	  Q     P   j     �     �     �     �  B     @   O  .   �     �     �  0   �  $        )     :     A  
   G  	   R     \  �  h  8   M  /   �  5   �  e   �  4   R	     �	     �	  V   �	  
   
     
  S   +
  t   
     �
       ?   #     c  j   s  P   �  `   /  
   �     �  G   �  8   �     1  	   D     N     Z  	   i     s                                                                             
                                     	                       A guided tour and greeter for GNOME. Change desktop layouts with Layout Switcher Click the time to see notifications Discover great apps through search, browsing and our recommendations in Manjaro Hello. Don't like it? Change it whenever you like! GNOME Tour GNOME Tour and Greeter. Get an overview of the system status and quickly change settings. Gnome;GTK; Greeter & Tour Hi there! Take the tour to learn your way around and discover essential features. In the activities view, just start typing to search for apps, settings and more. Just type to search Main Window Open Activities to launch apps The GNOME Project The activities view can also be used to switch windows and search. The notifications popover also includes personal planning tools. To get more advice and tips, see the Help app. Tour Tour Completed Use Add/Remove Software to find and install apps View system information and settings Welcome to {} {} _Close _Next _No Thanks _Previous _Start Tour Project-Id-Version: gnome-tour master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-tour/issues
PO-Revision-Date: 2020-10-30 08:11+0200
Last-Translator: Bogdan Covaciu <bogdan@manjaro.org>
Language-Team: Romanian <gnomero-list@lists.sourceforge.net>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Poedit 2.4.1
 O prezentare pe scurt despre GNOME şi un salut cordial. Schimbați aspectele desktop cu Layout Switcher Faceți clic pe oră pentru a vizualiza notificările Descoperă mai multe aplicații prin căutare, navigare și recomandările noastre în Manjaro Hello. Nu-ți place? Schimbați-l ori de câte ori doriți! Prezentare GNOME Salut și Prezentare GNOME. Obține o prezentare generală a stării sistemului și schimbă configurările rapid. Gnome;GTK; Salut și Prezentare Bună! Vizualizaţi prezentarea pentru a descoperi funcționalitățile esențiale. În interfața Activităților, doar începeți să tastați pentru a căuta aplicații, configurări și mai multe. Tastați pentru a căuta Fereastra principală Lansarea aplicațiilor se face prin intermediul Activităţilor Proiectul GNOME Modificarea poziției ferestrelor si Căutarea se poate face, de asemenea, prin accesarea Activităților. Popover-ul de notificări include, de asemenea, unelte de planificare personale. Pentru a obține mai multe sfaturi și îndrumări, consultați secțiunea Ajutor a aplicației. Prezentare Finalul Prezentării Utilizează Add/Remove Software pentru a găsi și a instala aplicații Vizualizează informațiile și configurările de sistem Bun venit la {} {} În_chide _Următorul _Nu mulțumesc _Anterior Î_ncepe turul 